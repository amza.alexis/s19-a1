/*Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.
	Log the 2 new Dog objects in the console or alert.

  let student1 = {
	name: "Shawn Michaels"
	birthday: "May 5, 2003',
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101" "Social Sciences 201"]
}

let student2 = {
	name "Steve Austin",
	birthday: "June 15, 2001",
	age: 20
	isEnrolled: true
	classes: ["Philosphy 401", "Natural Sciences 402",
}

function introduce(student){

	//Note: You can destructure objects inside functions.

	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses + " classes);
}

*/

let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

//object destructuring
introduce({
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
});

function introduce({name, age, classes}){

	//template literals
	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}

//Arrow function
const getCube = (num) => Math.pow(num, 3);
let cube = getCube(3);
console.log(cube);

let numArr = [15,16,32,21,21,2];
numArr.forEach(num => console.log(num));

const numSquared = numArr.map(num => Math.pow(num, 2));
console.log(numSquared);


class Dog{
	constructor(name, breed, dogAge, humanYears){
		this.name = name;
		this.breed = breed;
		this.dogAge = 7 * humanYears;
		this.humanYears = humanYears;
	}
};

let dog1 = new Dog('Aeros', 'Shih-Tzu', 2, 3);
console.log(dog1);
let dog2 = new Dog('Jiko', 'Labrador', 1, 7);
console.log(dog2);
